from django.urls import path
from .views import api_hat_list, api_hat_delete

urlpatterns = [
    path("hat_list/", api_hat_list, name="api_hat_list"),
    path("hat_delete/<int:pk>/", api_hat_delete, name="api_hat_delete"),
]
