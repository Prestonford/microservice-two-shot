from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import Hat, LocationVO
import json
from common.json import ModelEncoder
from django.http import JsonResponse

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["style_name", "picture_url", "fabric", "color", "id"]

    def get_extra_data(self, o):
        return {"location": o.location.import_href}


class HatLocationEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "section_number", "shelf_number"]

@require_http_methods(["GET", "POST"])
def api_hat_list(request,):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )

    else:
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(id=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatListEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE"])
def api_hat_delete(request, pk):
    if request.method == "GET":
        try:
            location = LocationVO.objects.get(id=pk)
            return JsonResponse(
                location,
                encoder=HatLocationEncoder,
                safe=False
            )
        except LocationVO.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    else:
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
