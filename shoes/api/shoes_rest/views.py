import json
from django.shortcuts import render
from .models import Shoe, BinVO
from common.json import ModelEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods

# Create your views here.
class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "name",
        "import_href"
    ]


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "model_name",
        "manufacturer",
        "color",
        "picture_url",
        "id",
        "bin"
    ]
    encoders = {
        "bin" : BinVODetailEncoder(),
    }

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "model_name",
        "id",
        "picture_url",
        "color",
        "manufacturer"
    ]
    def get_extra_data(self, o):
        return {"bin": o.bin.name,
                "bin_id": o.bin.import_href}


@require_http_methods(["GET", "POST"])
def list_shoes(request, bin_vo_id=None):
    if request.method=="GET":
        if (bin_vo_id==None):
            shoes = Shoe.objects.all()
        else:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(id=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE"])
def show_shoe(request, pk):
    if request.method=="GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )
    else: #DELETE

        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted" : count >0}
        )
