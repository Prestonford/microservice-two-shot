function HatDelete(props) {
    return (
        <div className="px-4 py-5 my-5 mt-0">
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Style</th>
                        <th>Picture</th>
                        <th>Location</th>
                    </tr>
                </thead>
                <tbody>
                    {props.hats?.map((hat) => {
                        return (
                            <tr key={hat.id}>
                                <td>{hat.style_name}</td>
                                <td>{hat.location}</td>
                                <td>
                                    <img
                                        className="img img-fluid"
                                        src={hat.picture_url}
                                        alt={hat.style_name}
                                        width={200}
                                        height={200}
                                    />
                                </td>
                                <td>
                                    <button
                                        onClick={async () => {
                                            const url = `http://localhost:8090/api/hats/${hat.id}/`;
                                            const response = await fetch(url, {
                                                method: "delete",
                                                headers: { "Content-Type": "application/json" },
                                            });
                                            if (response.ok) {
                                                window.location.reload();
                                            }
                                        }}
                                        className="delete-btn"
                                    >
                                        Delete
                                    </button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default HatDelete;
