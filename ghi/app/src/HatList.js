function HatList(props) {
    return (
        <div className="px-4 py-5 my-5 mt-0">
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Style</th>
                        <th>Fabric</th>
                        <th>Color</th>
                        <th>Picture</th>
                        <th>Location</th>
                    </tr>
                </thead>
                <tbody>
                    {props.hats?.map((hat) => {
                        return (
                            <tr key={hat.id}>
                                <td>{hat.style_name}</td>
                                <td>{hat.fabric}</td>
                                <td>{hat.color}</td>
                                <td>{hat.location}</td>
                                <td>
                                    <img
                                        className="img img-fluid"
                                        src={hat.picture_url}
                                        alt={hat.style_name}
                                        width={200}
                                        height={200}
                                    />
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <div>
                <button className="create-btn">
                    onclick={async () => {
                    const url = 'http://localhost:8090/api/shoes/${hat.id}/'
                    const response = await fetch(url, {
                        //I fully understand this is bad and I am sorry
                </button>
                }}
            </div>
        </div>
    );
}

export default HatsList;
