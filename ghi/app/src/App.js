import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import ShoesList from "./ShoesList";
import CreateShoe from "./CreateShoe";
import HatList from "./HatList";
import HatDelete from "./HatDelete";

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route
            path="/shoes/"
            index
            element={<ShoesList shoes={props.shoes} />}
          />
          <Route path="/shoes/new" element={<CreateShoe />} />
          <Route
            path="/hats/"
            index
            element={<HatList hats={props.hats} />}
          />
          <Route path="/hats/delete" element={<HatDelete/>} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
