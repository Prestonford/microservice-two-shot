function ShoesList(props) {
  return (
    <div className="px-4 py-5 my-5 mt-0">
      <table className="table">
        <thead>
          <tr>
            <th>Shoe Model</th>
            <th>Manufacturer</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Bin name</th>
          </tr>
        </thead>
        <tbody>
          {props.shoes?.map((shoe) => {
            return (
              <tr key={shoe.id}>
                <td>{shoe.model_name}</td>
                <td>{shoe.manufacturer}</td>
                <td>{shoe.color}</td>
                <td>
                  <img
                    className="img img-fluid"
                    src={shoe.picture_url}
                    alt={shoe.model_name}
                    width={200}
                    height={200}
                  />
                </td>
                <td>{shoe.bin}</td>
                <td>
                  <button
                    onClick={async () => {
                      const url = `http://localhost:8080/api/shoes/${shoe.id}/`;
                      const response = await fetch(url, {
                        method: "delete",
                        headers: { "Content-Type": "application/json" },
                      });

                      if (response.ok) {
                        window.location.reload();
                      }
                    }}
                    className="delete-btn"
                  >
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default ShoesList;
